import { NativeBaseProvider } from 'native-base';

import messaging from '@react-native-firebase/messaging';

import React, {useEffect} from 'react';



import Navigation from './src/navigation/Index';
import Authprovider from './src/providers/AuthProvider';


const App = () => {

  useEffect(()=>{
    const foregroundSuscriber = messaging().onMessage(async remoteMessage =>{
      console.log('notificación' , remoteMessage)
    });

   const topicSuscriber = messaging().subscribeToTopic("6216c4fc1b4c14ca203d7282")
    .then(() => {console.log("suscrito a 6216c4fc1b4c14ca203d7282")});

   const backgroundSuscriber = messaging().setBackgroundMessageHandler(async(remoteMessage) => {
      console.log("background message", remoteMessage);
    } );

    return () => {
      foregroundSuscriber();
      topicSuscriber();
      backgroundSuscriber();
    };

  },[])

  return (
    <NativeBaseProvider>
      <Authprovider>
          <Navigation/>
      </Authprovider>
    </NativeBaseProvider>
      
    
  );
}

export default App;
