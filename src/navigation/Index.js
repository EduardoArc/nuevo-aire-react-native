import React, {useState, useEffect} from 'react';
import {Text, View} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import AsyncStorage from '@react-native-async-storage/async-storage';

import Login from '../screens/Login';
import Index from '../screens/Index';
import Home from '../screens/Home';
import Devicedetails from '../screens/DeviceDetails';
import Devices from '../screens/Devices';
import useAuth from '../providers/useAuth';
import DeviceComponent from '../components/DeviceComponent';
import Information from '../screens/Information';



const Navigation = () => {
  const Stack = createStackNavigator();
  const Tab = createBottomTabNavigator();

  const auth = useAuth();

 // useEffect( ()=>{console.log(auth.isAdmin())},[] )

  if (auth.isLogged()) {
   
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="index"
            component={Index}
            options={{headerShown: false}}
          />

          <Stack.Screen name="detail" component={Devicedetails} options={{headerShown: false}} />
          <Stack.Screen name="information" component={Information} options={{headerShown: false}} />
        </Stack.Navigator>

        {/* <Tab.Navigator
          initialRouteName={Home}
          screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
              let iconName;
              let rn = route.name;

              if (rn === 'Inicio') {
                iconName = focused ? 'home' : 'home-outline';
              } else if (rn === 'Dispositivos') {
                iconName = focused ? 'list' : 'list-outline';
              }

              return <Ionicons name={iconName} size={size} color={color} />;
            },
          })}>
          <Tab.Screen
            name="Inicio"
            component={Home}
            options={{headerShown: false}}
          />
          <Tab.Screen
            name="Dispositivos"
            component={Devices}
            options={{headerShown: false}}
          />
          
          
        </Tab.Navigator>*/}
      </NavigationContainer>
    );
  } else {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="login"
            component={Login}
            options={{headerShown: false}}
          />
          
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
};

export default Navigation;
