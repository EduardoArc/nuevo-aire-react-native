import React from 'react';
import {Text, View} from 'native-base';
import {Center, NativeBaseProvider, Heading, Button} from 'native-base';
import {ScrollView, SafeAreaView, FlatList} from 'react-native';
import InfoComponent from '../components/InfoComponent';

const Information = () => {
  const parametros = [
    {
      pm10: '0-149',
      pm25: '0-50',
      condicion: 'Buena',
      recomendacion: 'Se puede realizar cualquier actividad al aire libre.',
    },

    {
      pm10: '150-194',
      pm25: '51-79',
      condicion: 'Regular',
      recomendacion:
        'Las personas vulnerables deben considerar limitar esfuerzos prolongados al aire libre.',
    },

    {
      pm10: '195-239',
      pm25: '80-109',
      condicion: 'Alerta',
      recomendacion:
        'La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre.',
    },

    {
      pm10: '240-329',
      pm25: '110-168',
      condicion: 'Pre-emergencia',
      recomendacion:
        'La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo.',
    },

    {
      pm10: '> 329',
      pm25: '> 169',
      condicion: 'Emergencia',
      recomendacion:
        'La población en general debe suspender los esfuerzos al aire libre.',
    },
  ];

  return (
    <NativeBaseProvider>
      <SafeAreaView>
        <FlatList
          ListHeaderComponent={() => (
            <Center mt="2"  p="5">
            <Heading>Colores calidad del aire</Heading>
            </Center>
          )}
          data={parametros}
          keyExtractor={item => item.id}
          renderItem={({item, index}) => <InfoComponent condicion = {item.condicion} recomendacion = {item.recomendacion} pm25={item.pm25} pm10={item.pm10} />}
        />
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default Information;
