import React, {useEffect, useState} from 'react';
import {
  Text,
  Center,
  NativeBaseProvider,
  Heading,
  Button,
  Link,
  Spinner,
  HStack,
} from 'native-base';
import {ScrollView, SafeAreaView, FlatList} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import CardDevice from '../components/CardDevice';
import Icon from 'react-native-vector-icons/Ionicons';
import useAuth from '../providers/useAuth';
import axios from 'axios';

const Index = ({navigation}) => {
  const [dispositivos, setDispositivos] = useState([]);
  const [cargados, setCargados] = useState(false);

  const auth = useAuth();

  useEffect(() => {
    getDispositivos();
    if (auth.isAdmin() == true) {
      console.log('es admin');
    } else {
      console.log('no es admin');
    }
  }, []);

  const Loading = () => {
    return (
      <Center flex={1} px="3">
        <HStack space={2} justifyContent="center">
          <Spinner accessibilityLabel="Loading posts" />
          <Heading color="primary.500" fontSize="md">
            Cargando
          </Heading>
        </HStack>
      </Center>
    );
  };

  const getDispositivos = async () => {
    //console.log("token ",auth.token)

    await axios
      .get(
        'http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/medicion',
        {headers: {'x-access-token': auth.token}},
      )
      .then(async response => {
        setDispositivos(response.data);
        console.log('DISPS ', response);
      })
      .finally(() => {
        setCargados(true);
      });

    const disps = await data.json();
    // console.log(disps);

    setDispositivos(disps);
  };

  return (
    <NativeBaseProvider>
      {!cargados && <Loading />}

      <SafeAreaView>
        {cargados && (
          <FlatList
            ListHeaderComponent={() => (
              <Center mt="2" p="5">
                <Heading>Dispositivos de medición</Heading>

                <Link
                  _text={{
                    color: 'blue.400',
                  }}
                  mt={4}
                  onPress={() => navigation.navigate('information')}>
                  <Icon color="#0284c7" name="information" size={30} />
                  Click aquí para obtener información sobre el indice de calidad
                  del aire (ICA)
                </Link>

                {dispositivos.length === 0 && (
                  <Center flex={1} px="3">
                  <Heading mt={4}>
                    No se encontraron dispositivos con mediciones registradas ⚠️
                  </Heading>
                  </Center>
                )}
              </Center>
            )}
            data={dispositivos}
            keyExtractor={item => item.id}
            renderItem={({item, index}) => (
              <CardDevice
                navigation={navigation}
                key={index}
                nombre={item.dispositivo.nombre}
                id={item.dispositivo._id}
                pm25={item.medicion.pm25}
                grupo={item.dispositivo.grupo.nombre}
                organizacion={item.dispositivo.grupo.organizacion.nombre}
              />
            )}
          />
        )}
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default Index;
