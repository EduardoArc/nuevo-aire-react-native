import React, {useState, useEffect} from 'react';
import { ScrollView, Text } from 'native-base';
import DeviceComponent from '../components/DeviceComponent';
import axios from 'axios';

const Devices = () => {
    const [dispositivos, setDispositivos] = useState([]);

    useEffect(()=>{
        console.log("asda algos")
         getDispositivos();
      
    }, [])

    const getDispositivos = async () =>{
      
        const data = await fetch('http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos');
        const disps = await data.json();
        console.log(disps)
        setDispositivos(disps);
        console.log(dispositivos)
    }
    return (
        <>
        <Text>Dispositivos</Text>
        <ScrollView>
        
        
       
         {dispositivos.map((d, idx) => (
              
              <DeviceComponent  key={idx} nombre={d.nombre}/>
            ))}

        </ScrollView>
        </>
    );
}

export default Devices;
