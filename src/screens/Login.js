import React, {useState, useEffect} from 'react';
import {
  Box,
  Text,
  Heading,
  VStack,
  FormControl,
  Input,
  Link,
  Button,
  HStack,
  Center,
  NativeBaseProvider,
} from 'native-base';

import {useNavigation} from '@react-navigation/core';
import {sha512} from 'react-native-sha512';
import axios from 'axios';
import useAuth from '../providers/useAuth';

export const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  

  const navigation = useNavigation();

  const auth = useAuth();
  const handleSubmit = async () => {

    await sha512(password).then(hash => {

      const user = {
        email: email.toLowerCase(),
        password: hash,
      };

       axios
        .post('http://new-air-api-dev.us-west-2.elasticbeanstalk.com/signin/', user)
        .then(response => {
          const resp = response.data;
          console.log("roles : ", resp.roles)
          const {token} = resp;
          auth.login(token, resp.roles)
         // AsyncStorage.setItem('token', JSON.stringify(token))
          
        }).catch(error => {
         console.log(error)
        });
  
    });
 
  };

  const handleChangeEmail = text => {
    setEmail(text);
  };
  const handleChangePassword = text => {
    setPassword(text);
  };

 

  return (
    <Box safeArea p="2" py="8" w="90%" maxW="290">
      <Heading
        size="lg"
        fontWeight="600"
        color="coolGray.800"
        _dark={{
          color: 'warmGray.50',
        }}>
        Bienvenido a Nuevo Aire!
      </Heading>
      <Heading
        mt="1"
        _dark={{
          color: 'warmGray.200',
        }}
        color="coolGray.600"
        fontWeight="medium"
        size="xs">
        Inicia sesión para continuar!
      </Heading>

      <VStack space={3} mt="5">
        <FormControl>
          <FormControl.Label>Email</FormControl.Label>
          <Input   value={email} onChangeText={text => handleChangeEmail(text)} />
        </FormControl>
        <FormControl>
          <FormControl.Label>Password</FormControl.Label>
          <Input
           
            value={password}
            onChangeText={text => handleChangePassword(text)}
            type="password"
          />
          
        
        </FormControl>
        <Button mt="2" colorScheme="indigo" onPress={handleSubmit}>
          Ingresar
        </Button>
        <HStack mt="6" justifyContent="center">
          <Text
            fontSize="sm"
            color="coolGray.600"
            _dark={{
              color: 'warmGray.200',
            }}>
            Leer terminos y condiciones de uso .{' '}
          </Text>
          <Link
            _text={{
              color: 'indigo.500',
              fontWeight: 'medium',
              fontSize: 'sm',
            }}
            href="#">
            Aquí
          </Link>
        </HStack>
      </VStack>
    </Box>
  );
};

export default () => {
  return (
    <NativeBaseProvider>
      <Center flex={1} px="3">
        <Login />
      </Center>
    </NativeBaseProvider>
  );
};
