import React, {useState, useEffect} from 'react';
import {
  Heading,
  HStack,
  VStack,
  Stack,
  Center,
  Text,
  View,
  NativeBaseProvider,
  Box,
  ScrollView,
  Flex,
  Switch,
} from 'native-base';
import Weekchart from '../components/WeekChart';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

const Devicedetails = ({route}) => {
  //route.params.itemId Obtiene id del dispositivo

  return (
    <NativeBaseProvider>
      <ScrollView
        _contentContainerStyle={{
          px: '20px',
          mb: '4',
          minW: '72',
        }}>
        <Header id={route.params.itemId} />

        <Mapa id={route.params.itemId} />

        <Weekchart mt="2" id={route.params.itemId} />
      </ScrollView>
    </NativeBaseProvider>
  );
};

const Header = props => {
  const [dispositivo, setDispositivo] = useState('');
  const [medicion, setMedicion] = useState('');
  const [suscrito, setSuscrito] = useState(false);

  //Obtiene dispositivo y medición
  const getDispositivo = async () => {
    const data = await fetch(
      'http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/' +
        props.id,
    );
    const disps = await data.json();
    // console.log(disps)
    setDispositivo(disps);
    //console.log("DISPOSITIVO: ",dispositivo)
  };

  const getMedicion = async () => {
    const data = await fetch(
      'http://new-air-api-dev.us-west-2.elasticbeanstalk.com/mediciones/ultima/' +
        props.id,
    );
    const disps = await data.json();
    //  console.log(disps)
    setMedicion(disps);
    // console.log("MEDICION: ",medicion)
  };

  useEffect(() => {
    getDispositivo();
    getMedicion();
    getSuscripcion();
  }, []);

  const getSuscripcion = async () =>{
    try {
      const value = await AsyncStorage.getItem(props.id)
      
      if(value !== null && value == "true") {

        setSuscrito(true);
       // console.log("el VALOR ✅"+value)

        

        
      }else{
        setSuscrito(false)
        //console.log("NO HAY DISP ⚠️")
      }

      console.log(value)
    } catch(e) {
      // error reading value
    }
  }

  const handleSwitch  = async (value, id) =>{
    
    setSuscrito(!suscrito);

    

    if(value){
      console.log("suscribe")
      try {
        //agrega disp al local storage
        await AsyncStorage.setItem(id, "true")

        //suscribir al dispositivo
        messaging().subscribeToTopic(props.id)
    .then(() => {console.log("suscrito a "+props.id)});

      } catch (e) {
        // saving error
      }

      
    }else{
      console.log("des-suscribe")
      //QUITA ITEM de local storage
      await AsyncStorage.setItem(id, "false")
      //des suscribe notificaciones

      messaging().unsubscribeFromTopic(props.id)
    .then(() => {console.log("des-suscrito a "+props.id)});
    }
    
  }

  return (
    <Stack space={1}>
      <Heading ml="2" mt="6" mb="4">
        {dispositivo && dispositivo.dispositivo.nombre}
      </Heading>

      {
        <HStack space={10}>
          <Text bold>Suscribir a notificaciones</Text>
          <Switch  isChecked={suscrito}  size="md" onToggle={ (value)=> handleSwitch(value, props.id)} />

        </HStack>
      }

      {medicion != '' && (
        <HStack space={1} alignItems="center">
          <DatoCuadrado
            nombre="PM 2.5"
            valor={medicion != '' && Math.trunc(medicion.medicion.pm25)}
          />

          <Center flex={1} px="3">
            <VStack space={3}>
              <DatoRectangular
                nombre="PM 10"
                valor={medicion != '' && Math.trunc(medicion.medicion.pm10)}
              />
              <DatoRectangular
                nombre="Co2    "
                valor={medicion && Math.trunc(medicion.medicion.co2)}
              />
              <DatoRectangular
                nombre="T°C      "
                valor={medicion && Math.trunc(medicion.medicion.temperatura)}
              />
            </VStack>
          </Center>
        </HStack>
      )}
      {medicion != '' && <MensajeRecomendacion pm25={medicion.medicion.pm10} />}
    </Stack>
  );
};

const MensajeRecomendacion = props => {
  const [mensaje, setMensaje] = useState(
    'Se puede realizar cualquier actividad al aire libre.',
  );

  useEffect(() => {
    if (props.pm25 < 50) {
      setMensaje('Se puede realizar cualquier actividad al aire libre.');
    }
    if (props.pm25 > 50) {
      setMensaje(
        'Las personas vulnerables deben considerar limitar esfuerzos prolongados al aire libre.',
      );
    }
    if (props.pm25 > 80) {
      setMensaje(
        'La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre.',
      );
    }
    if (props.pm25 > 110) {
      setMensaje(
        'La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo.',
      );
    }
    if (props.pm25 > 169) {
      setMensaje(
        'La población en general debe suspender los esfuerzos al aire libre.',
      );
    }
  }, []);
  return (
    <Center>
      <Text mt={4} mb={4}>
        {mensaje}
      </Text>
    </Center>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,

    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
    position: 'relative',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
});

const Mapa = props => {
  const [dispositivo, setDispositivo] = useState('');
  const [latitud, setLatitud] = useState('');
  const [longitud, setLongitud] = useState('');

  const getDispositivo = async () => {
    try {
      const data = await fetch(
        'http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/' +
          props.id,
      );
      const disps = await data.json();
      // console.log(disps.dispositivo.latitud)
      setDispositivo(disps);
      setLatitud(disps.dispositivo.latitud);
      setLongitud(disps.dispositivo.longitud);
     // console.log('DISP: ', disps);
      //console.log('lATITUD: ', latitud);
     // console.log('LONGITUD: ', longitud);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDispositivo();
  }, []);

  return (
    <Center flex={1} px="3">
      <Box width="100%" size="350">
        {longitud != '' && (
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            region={{
              latitude: parseFloat(latitud),
              longitude: parseFloat(longitud),
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}>
            <Marker
              coordinate={{
                latitude: parseFloat(latitud),
                longitude: parseFloat(longitud),
              }}
              image={{
                uri: 'https://upload.wikimedia.org/wikipedia/commons/f/fb/Map_pin_icon_green.svg',
              }}
              title={dispositivo && dispositivo.organizacion.nombre}
              description={dispositivo && dispositivo.grupo.nombre}
            />
          </MapView>
        )}
      </Box>
    </Center>
  );
};

const DatoCuadrado = params => {
  const [color, setColor] = useState('success.600');

  useEffect(() => {
    if (params.valor < 50) {
      setColor('success.600');
    }
    if (params.valor > 50) {
      setColor('yellow.500');
    }
    if (params.valor > 80) {
      setColor('warning.600');
    }
    if (params.valor > 110) {
      setColor('red.600');
    }
    if (params.valor > 169) {
      setColor('fuchsia.700');
    }
  }, []);

  return (
    <Box width="100%" bg={color} rounded="md" size="150" safeArea>
      <VStack p="9" space={1} alignItems="center">
        <Heading color="warmGray.50">{params.nombre}</Heading>
        <Heading color="warmGray.50">{params.valor}</Heading>
      </VStack>
    </Box>
  );
};

const DatoRectangular = params => {
  const [color, setColor] = useState('success.600');
  const [textColor, setTextColor] = useState('warmGray.50');
  useEffect(() => {
    if (params.nombre == 'PM 10') {
      if (params.valor < 149) {
        setColor('success.600');
      }
      if (params.valor > 149) {
        setColor('yellow.500');
      }
      if (params.valor > 195) {
        setColor('warning.600');
      }
      if (params.valor > 240) {
        setColor('red.600');
      }
      if (params.valor > 329) {
        setColor('fuchsia.700');
      }
    } else {
      setColor('warmGray.300');
      setTextColor('trueGray.700');
    }
  }, []);
  //regular color según tipo de dato
  return (
    <Box minW="100%" maxW="110%" h="10" bg={color} rounded="md">
      <HStack pt="2" ml="2" justify="center">
        <Text pr="55%" color={textColor} bold>
          {params.nombre}
        </Text>
        <Text pr="15%" color={textColor} bold>
          {params.valor}
        </Text>
      </HStack>
    </Box>
  );
};

export default Devicedetails;
