import React, {createContext, useState, useEffect} from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

export const AuthContext = createContext();


const Authprovider = ({children}) => {
    
    const [token, setToken] = useState(null);

    const [roles, setRoles] = useState(null);

   
  

    const contextValue = {
        token,
        roles,
        login(tok, roles){
            setToken(tok);
            setRoles(roles);
          

        },
        logout(){
            setToken(null)
        },
        isLogged(){
            //return !!token;
            
            if(token == null || token == "null"){
                return false;
            }
            return true
           
        },
        isAdmin() {
            //  const roles = localStorage.setItem("roles", roles);
              //TODO: recorre roles del usuario hasta encontrar admin
              //getUsuario()
  
              
              if(roles){
                  for(let i = 0; i < roles.length; i++){
                      if(roles[i].name === "admin"){
                          return true
                      }
                      
                  }
              }
             
              return false;
          },
      
        
          

    }

    return <AuthContext.Provider value={contextValue}>
        {children}
    </AuthContext.Provider>
}

export default Authprovider;
