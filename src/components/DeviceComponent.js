import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';

import {
  Text,
  Box,
  Stack,
  AspectRatio,
  Link,
  Divider,
  HStack,
  Center,
  Heading,
  Switch,
  useColorMode,
  NativeBaseProvider,
  extendTheme,
  VStack,
  Code,
  Button
} from 'native-base';

// Define the config
const config = {
    useSystemColorMode: false,
    initialColorMode: 'dark',
  };
  
  // extend the theme
  export const theme = extendTheme({config});

  const PollutionColor = (props ) =>{
    // console.log(props)
     //COLORES DE TEMPERATURA
      if(props.unit === "temp"){
       
        if(props.value >= 30){
          return ( <Box mt={1} rounded="xl" width="100" height="2" backgroundColor="danger.700" /> )
        }else if(props.value >= 20){
          return ( <Box mt={1} rounded="xl" width="100" height="2" backgroundColor="amber.400" /> )
        }else{
          return ( <Box mt={1} rounded="xl" width="100" height="2" backgroundColor="success.500" /> )
        }
    
        return ( <Box mt={1} rounded="xl" width="100" height="2" backgroundColor="success.500" /> )
    
      //COLORES HUMEDAD
      }else if(props.unit === "hume"){
    
      }
      
      else{
        return (
          <Box
              mt={1}
              rounded="xl"
              width="100"
              height="2"
              backgroundColor="amber.400"
            />
        )
      }
    
      
    
    
    
      //return "danger.600"
    }

    

const DeviceComponent = (props) => {
    const [users, setUsers] = useState([]);
  
  const {colorMode, toggleColorMode} = useColorMode();

  useEffect(() => {
    getUsers();
   
  }, []);

  const getUsers = async () => {
    const data = await fetch('http://10.0.2.2:5000/mediciones/619730e4d242de2f6336519e');
    const users = await data.json();
    console.log(users)
    setUsers(users);
  };


  


  return (
    <NativeBaseProvider>
      
      <Box
        rounded="xl"
        overflow="hidden"
        m={3}
        shadow={4}
        _light={{backgroundColor: 'gray.50'}}
        _dark={{backgroundColor: 'gray.700'}}>
        <Stack p="4" space={3}>
          <Stack space={2}>
            <View style={styles.weatherItem}>
              <Heading>{props.nombre}</Heading>

              <HStack
                alignItems="center"
                space={4}
                justifyContent="space-between">
                <Divider my="4" thickness="2" orientation="vertical" />
                <HStack alignItems="center">
                <Button size="sm" onPress={()=>{navigation.navigate('deviceDetails')}} >Ver más</Button>
                </HStack>
              </HStack>
            </View>

            <View
              style={{
                borderBottomColor: 'darkgray',
                borderBottomWidth: 1,
              }}
            />
          </Stack>

          <View >
          <VStack space={1} alignItems="center">
      


 
          </VStack>
             
              

            
          </View>
        </Stack>
      </Box>
      
    </NativeBaseProvider>
  );
}
export default DeviceComponent;
const styles = StyleSheet.create({
    container: {
      flex: 1.5,
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 15,
    },
    heading: {
      fontSize: 45,
      color: 'white',
      fontWeight: '100',
    },
    subheading: {
      fontSize: 25,
      color: 'white',
      fontWeight: '300',
    },
    rightAlign: {
      textAlign: 'right',
      marginTop: 20,
    },
    timezone: {
      fontSize: 20,
      color: 'white',
    },
    latlong: {
      fontSize: 16,
      color: 'white',
      fontWeight: '700',
    },
    weatherItemContainer: {
      backgroundColor: '#18181b99',
      borderRadius: 10,
      padding: 10,
      marginTop: 10,
    },
    weatherItem: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    weatherItemTitle: {
      color: '#eee',
      fontSize: 14,
      fontWeight: '100',
    },
  });
  


