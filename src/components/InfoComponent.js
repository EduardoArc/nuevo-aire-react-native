import React, {useState, useEffect} from "react"
import {
  Box,
  Heading,
  AspectRatio,
  Image,
  Text,
  Center,
  HStack,
  Stack,
  NativeBaseProvider,
} from "native-base"

const InfoComponent = (props) => {

    const [color, setColor] = useState("success.600");
  

  useEffect(() => {
  if (props.condicion ==  "Buena") {
      setColor("success.600");
    }
    if (props.condicion ==  "Regular") {
      setColor("yellow.500");
    }
    if (props.condicion ==  "Alerta") {
      setColor("warning.600");
    }
    if (props.condicion ==  "Pre-emergencia") {
      setColor("red.600");
    }
    if (props.condicion ==  "Emergencia") {
      setColor("fuchsia.700");
    }
  }, []);

  return (
    <Center flex={1} px="3" m="2">
    <Box
      maxW="100%"
      minW="100%"
      rounded="lg"
      overflow="hidden"
      borderColor="coolGray.200"
      borderWidth="1"
      _dark={{
        borderColor: "coolGray.600",
        backgroundColor: "gray.700",
      }}
      _web={{
        shadow: 2,
        borderWidth: 0,
      }}
      _light={{
        backgroundColor: color,
      }}
    >
      
      <Stack p="4" space={3}>
        <Stack space={2}>
        <Heading size="lg" ml="-1" >
            {props.condicion}
          </Heading>
          

          
          <Text
            fontSize="xs"
            _light={{
              color: "white",
            }}
            _dark={{
              color: "white",
            }}
            fontWeight="500"
            ml="-0.5"
            mt="-1"
          >
            PM 2.5 : {props.pm25}
          </Text>

          <Text
            fontSize="xs"
            _light={{
              color: "white",
            }}
            _dark={{
              color: "white",
            }}
            fontWeight="500"
            ml="-0.5"
            mt="-1"
          >
            PM 10  : {props.pm10}
          </Text>
        </Stack>
        <Text fontWeight="400" color="white">
          {props.recomendacion}
        </Text>
        <HStack alignItems="center" space={4} justifyContent="space-between">
         
        </HStack>
      </Stack>
    </Box>
    </Center>
  );
};

export default InfoComponent;
