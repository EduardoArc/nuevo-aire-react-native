import React, {useState, useEffect} from "react"
import {
  Box,
  Heading,
  AspectRatio,
  Image,
  Text,
  Center,
  HStack,
  Stack,
  NativeBaseProvider,
  Button,
 
} from "native-base"
export const CardDevice = (props) => {
  
  return (
    <NativeBaseProvider>
    <Center flex={1} px="3" m="5">
    <Box
    safeArea
    
      maxW="80"
      rounded="lg"
      overflow="hidden"
      borderColor="coolGray.200"
      borderWidth="1"
      _dark={{
        borderColor: "coolGray.600",
        backgroundColor: "gray.700",
      }}
      _web={{
        shadow: 5,
        borderWidth: 0,
      }}
      _light={{
        backgroundColor: "gray.50",
      }}
    >
      <Box>
        <AspectRatio w="100%" ratio={16 / 9}>
          <Image
            source={{
              uri: "https://www.holidify.com/images/cmsuploads/compressed/Bangalore_citycover_20190613234056.jpg",
            }}
            alt="image"
          />
        </AspectRatio>

        <Pm25 pm25={props.pm25}/>

      </Box>
      <Stack p="4" space={3}>
        <Stack space={2}>
        <Heading size="md" ml="-1">
            {props.organizacion}
          </Heading>
          <Heading size="xs" ml="-1">
            {props.nombre}
          </Heading>
          <Heading size="xs" ml="-1">
            {props.grupo}
          </Heading>
          <Calidad pm25={props.pm25}/>

        </Stack>
        <Mensaje  pm25={props.pm25}/>
        <HStack alignItems="center" space={4} justifyContent="space-between">
          <HStack alignItems="center">
          <Button size="sm" variant="ghost" onPress={()=>{
            props.navigation.push('detail', {
              itemId: props.id,
            })
          }}>
            Más información
          </Button>
          </HStack>
        </HStack>
      </Stack>
    </Box>
    </Center>
    </NativeBaseProvider>
  )
}
const Calidad = (props) => {
  const [mensaje, setMensaje] = useState("Buena")
  const [color, setColor] = useState("success.600");

    useEffect(() => {
        if (props.pm25 < 50) {
            setMensaje("Buena");
          }
          if (props.pm25 > 50) {
            setMensaje("Regular");
            setColor("yellow.500");
          }
          if (props.pm25 > 80) {
            setMensaje("Alerta");
            setColor("warning.600");
          }
          if (props.pm25 > 110) {
            setMensaje("Pre-emergencia");
            setColor("red.600");
          }
          if (props.pm25 > 169) {
            setMensaje("Emergencia");
            setColor("fuchsia.700");
          }
        }, []);


        return (
          <Stack space={3}  >
            <HStack space={3} >
          <Text
          fontSize="xs"
          _light={{
            color: "light.900",
          }}
          _dark={{
            color: "violet.400",
          }}
          fontWeight="500"
          ml="-0.5"
          mt="-1"
        >
          Condición : 
        </Text>
       
          <Text
            fontSize="xs"
            _light={{
              color: color,
            }}
            _dark={{
              color: "violet.400",
            }}
            fontWeight="500"
            ml="-0.5"
            mt="-1"
          >
            {mensaje}
          </Text>
          </HStack>
          </Stack>
        )

}

const Mensaje = (props) => {
  const [mensaje, setMensaje] = useState("Se puede realizar cualquier actividad al aire libre")

    useEffect(() => {
        if (props.pm25 < 50) {
            setMensaje("Se puede realizar cualquier actividad al aire libre");
          }
          if (props.pm25 > 50) {
            setMensaje("Las personas vulnerables deben considerar limitar esfuerzos prolongados al aire libre");
          }
          if (props.pm25 > 80) {
            setMensaje("La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre");
          }
          if (props.pm25 > 110) {
            setMensaje("La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo");
          }
          if (props.pm25 > 169) {
            setMensaje("La población en general debe suspender los esfuerzos al aire libre");
          }
        }, []);
  return (
    <Text fontWeight="400">
    {mensaje}
  </Text>
  )
}

const Pm25 = (props) => {

  const [color, setColor] = useState("success.600");
  

  useEffect(() => {
  if (props.pm25 < 50) {
      setColor("success.600");
    }
    if (props.pm25 > 50) {
      setColor("yellow.500");
    }
    if (props.pm25 > 80) {
      setColor("warning.600");
    }
    if (props.pm25 > 110) {
      setColor("red.600");
    }
    if (props.pm25 > 169) {
      setColor("fuchsia.700");
    }
  }, []);



  return(


    <Center
    bg={color}
    _dark={{
      bg: color,
    }}
    _text={{
      color: "warmGray.50",
      fontWeight: "700",
      fontSize: "xs",
    }}
    position="absolute"
    bottom="0"
    px="3"
    py="1.5"
  >
   <Text  color="warmGray.50" >ICA : {props.pm25}</Text> 
  </Center>
  
  )
}

export default CardDevice;
