import React, {useState, useEffect} from 'react';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';

import {Center} from 'native-base';

import {View, Dimensions} from 'react-native';
import {Text} from 'native-base';

const Weekchart = props => {
  const [titulos, setTitulos] = useState([]);
  const [pm25, setPm25] = useState([0, 0, 0, 0, 0, 0, 0]);
  const [pm10, setPm10] = useState([0, 0, 0, 0, 0, 0, 0]);

  const data = {
    labels: titulos,
    datasets: [
      {
        data: pm25,
        color: (opacity = 1) => `rgba(134, 65, 244)`, // optional
        strokeWidth: 2, // optional
      },

      {
        data: pm10,
        color: (opacity = 1) => `rgba(134, 67, 0)`, // optional
        strokeWidth: 2, // optional
      },
    ],
    legend: ['PM 2.5', 'PM 10'], // optional
  };

  const getPromedios = async () => {
    try {
      const data = await fetch(
        'http://new-air-api-dev.us-west-2.elasticbeanstalk.com/mediciones/promedio/semanal/' +
          props.id,
      );
      const proms = await data.json();

     // console.log('PROMEDIOS ', proms);
      
      

      
      let titulosAux = [];
      let pm25Aux = [];
      let pm10Aux = [];

      proms.forEach(element => {
        let tituloCortado = element.name.split("-");
        titulosAux.push(tituloCortado[1]+ "-"+ tituloCortado[2]);
        pm25Aux.push(parseInt(element.pm25));
        pm10Aux.push(parseInt(element.pm10));
      });

      setTitulos(titulosAux);
      setPm25(pm25Aux)
      setPm10(pm10Aux)

      console.log('DATA ', datos);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getPromedios();
  }, []);

  const screenWidth = Dimensions.get('window').width;

  return (
    <Center flex={1} px="3" mt={5} mb={4}>
      <Text fontSize="lg" bold>
        Promedios díarios de los 7 días anteriores
      </Text>
      <View m={2}>
        <LineChart
          data={data}
          width={350}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#004282',
            backgroundGradientTo: '#1a91ff',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16,
            },
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#ffa726',
            },
          }}
        />
      </View>
    </Center>
  );
};

export default Weekchart;
